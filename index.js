'use strict';
module.exports = {
    sha1(plainTextPassword){
        const crypto = require('crypto');
        const hasher = crypto.createHash('sha1');
        hasher.update(plainTextPassword);
        return hasher.digest().toString('base64');
    },

    compareSha1(passwordGiven, passwordSha1){
        return this.sha1(passwordGiven) === passwordSha1;
    }
};
